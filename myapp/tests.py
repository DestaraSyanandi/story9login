from django.test import TestCase, Client
from django.urls import resolve

from .views import index,loginView,logoutView

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time



# Unit Test
class MyappUnitTest(TestCase):
    def test_myapp_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_myapp_using_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_myapp_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_can_access_login_with_no_myapp_using_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_can_access_login_with_no_myapp_using_login_view_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginView)

    # def test_cannot_access_logout_with_no_auth(self):
    #     response = Client().get('/logout/')
    #     self.assertEqual(response.status_code, 404)



class MyappFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(MyappFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(MyappFunctionalTest, self).tearDown()


    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url )

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn( "Selamat Datang di " ,title.text)
        time.sleep(10)


    def test_no_auth_can_access_login(self):
        self.browser.get(self.live_server_url + "/login/")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn("LOGIN", title.text)
        time.sleep(10)


        # def test_can_login(self):
        #     # Create user
        #     User.objects.create_user(username='nandii04', password='letsgo111')

        #     self.browser.get(self.live_server_url + "/login/")

        #     username = self.browser.find_element_by_id('username_login')
        #     password = self.browser.find_element_by_id('password_login')
        #     submit = self.browser.find_element_by_id('submit_login')

        #     username.send_keys('nandii04')
        #     password.send_keys('letsgo111')
        #     submit.click()
        #     time.sleep(10)

        #     self.assertIn("Selamat Datang di ", self.browser.page_source)
        #     time.sleep(10)
    
