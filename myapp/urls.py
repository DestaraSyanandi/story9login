from django.urls import path
from django.contrib import admin

from .views import index, IndexView, loginView, logoutView

urlpatterns = [
    path('logout/', logoutView,name="logout"),
    path('login/', loginView,name="login"),
    path('', index , name="index"),

    
]
